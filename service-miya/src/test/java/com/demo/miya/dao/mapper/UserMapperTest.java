package com.demo.miya.dao.mapper;

import com.alibaba.fastjson.JSON;
import com.demo.miya.model.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserMapperTest {

    @Resource
    private UserMapper userMapper;

    /**
     * 测试读写分离，读有多个db
     * 初始化数据
     * ds0库：INSERT INTO USER (user_id, user_name) VALUES ('1', 'ds0.user_name');
     * ds1库：INSERT INTO USER (user_id, user_name) VALUES ('1', 'ds1.user_name');
     */
    @Test
    public void testQuery() {
        for (int i = 0; i < 10; i++) {
            List<User> users = userMapper.queryByUserId(1L);
            log.info(JSON.toJSONString(users));
        }
    }
}
