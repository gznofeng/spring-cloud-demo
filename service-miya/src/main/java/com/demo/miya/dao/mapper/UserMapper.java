package com.demo.miya.dao.mapper;

import com.demo.miya.model.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserMapper {
    int deleteByPrimaryKey(Long id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    @ResultMap("BaseResultMap")
    @Select("select * from user where user_id = #{userId}")
    List<User> queryByUserId(@Param("userId") Long userId);
}