package com.demo.miya.controller;

import com.alibaba.fastjson.JSON;
import com.demo.controller.client.ServiceClient;
import com.demo.model.User;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Slf4j
@RestController
public class ServiceMiyaControler  {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Resource
	private ServiceClient serviceClient;

	@GetMapping("/hi")
	public String hi() {
		logger.info("servic-miya服务hi接口被调用");
		return "hi i'm miya!";
	}

	@GetMapping("/testServiceLunaClient")
	public String testServiceLunaClient() {
		return serviceClient.testFeign();
	}

	@PostMapping("/respBody")
	public String respBody(@RequestBody User user) throws InterruptedException {
		log.info("请求参数：{}", JSON.toJSONString(user));
		Thread.sleep(1000L);
		user.setId(1L);
		user.setUserId(2L);
		user.setUserName("userName");
		return JSON.toJSONString(user);
	}

	@PostMapping("/respBodyException")
	public String respException(@RequestBody User user) throws Exception {
		log.info("请求参数：{}", JSON.toJSONString(user));
		Thread.sleep(1000L);
		throw new Exception("异常");
	}
}
