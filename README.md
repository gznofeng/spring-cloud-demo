# spring-cloud-demo

## nacos
* 服务注册中心和配置中心使用nacos
* 官网：https://nacos.io/zh-cn/index.html

## service-luna
* 业务代码：http://localhost:8763/hi?name=forezp
* 功能：
    * 使用nacos作为服务注册中心和配置中心
    * 使用elasticsearch
    * 使用shardingsphere实现分库规则 (搭建shardingsphere proxy服务，可以将分库聚合成一个库，通过工具方便查询)
    * 启动命令添加skywalking探针，java -javaagent:x:\xxx\skywalking-agent\skywalking-agent.jar -jar your_application.jar
	* druid监控，相关配置在com.demo.config.servlet，监控url：http://localhost:8763/druid/login.html
	* 使用kafka订阅canal得到的数据库日志解析

## service-miya
* 业务代码

## service-gateway
* 使用springcloud gateway作为网管，实现统一路由方式
* 功能：
    * 使用modify request body拦截器，实现解释token后，将uid赋值到body对象里
    * 接入阿里Sentinel实现网关流控

## service-job
* 分布式定时任务工程，配置xxl-job-admin使用

## sentinel-dashboard
* sentinel控制台
* 修改sentinel控制台，将配置规则推送到nacos，使规则持久化
* 修改代码
  * GatewayFlowRuleController，流控规则持久化
  * DegradeController 降级规则持久化
  * 新增类型
    * com.alibaba.csp.sentinel.dashboard.config.NacosConfig
    * com.alibaba.csp.sentinel.dashboard.rule.GatewayDegradeRuleNacosProvider
    * com.alibaba.csp.sentinel.dashboard.rule.GatewayDegradeRuleNacosPublisher
    * com.alibaba.csp.sentinel.dashboard.rule.GatewayFlowRuleNacosProvider
    * com.alibaba.csp.sentinel.dashboard.rule.GatewayFlowRuleNacosPublisher
  

## ELK 日志分析系统
* ELK是Elasticsearch、Logstash、Kibana的简称，这三者是核心套件
    * Elasticsearch是实时全文搜索和分析引擎，提供搜集、分析、存储数据三大功能
    * Logstash是一个用来搜集、分析、过滤日志的工具。它支持几乎任何类型的日志，包括系统日志、错误日志和自定义应用程序日志 (官方推荐使用filebeat读取日志，然后推送到logstash进行日志过滤或直接推送到Elasticsearch)
    * Kibana是一个基于Web的图形界面，用于搜索、分析和可视化存储在 Elasticsearch指标中的日志数据
* filebeat用于采集应该日志

## skywalking
* SkyWalking 是观察性分析平台和应用性能管理系统。提供分布式追踪、服务网格遥测分析、度量聚合和可视化一体化解决方案
* 部署：在bin目录下执行startup.bat或startup.sh即可启动服务，执行startup.bat之后会启动如下两个服务
	* Skywalking-Collector：追踪信息收集器，通过 gRPC/Http 收集客户端的采集信息 ，Http默认端口 12800，gRPC默认端口 11800。
	* Skywalking-Webapp：管理平台页面 默认端口 8080，登录信息 admin/admin
* Agent使用，Skywalking 采用 Java 探针技术，对客户端应用程序没有任何代码侵入，使用起来简单方便，当然其具体实现就是需要针对不同的框架及服务提供探针插件。使用命令：
	* java -javaagent:G:\github\incubator-skywalking\skywalking-agent\skywalking-agent.jar -jar your_application.jar
* Agent过滤URL，在SkyWalking的追踪信息页面不再显示（参考：https://www.jianshu.com/p/4bd310850dd0）
* Skywalking的traceId与日志组件(log4j,logback,elk等)的集成，以logback为例，只要在日志配置xml中增加以下配置,则在打印日志的时候，自动把当前上下文中的traceId加入到日志中去，SkyWalking默认traceId占位符是%tid
```
<appender name="console" class="ch.qos.logback.core.ConsoleAppender">
    <layout class="org.apache.skywalking.apm.toolkit.log.logback.v1.x.TraceIdPatternLogbackLayout">
        <pattern>
            %d{yyyy-MM-dd HH:mm:ss} [%thread] %-5level %logger{36} - %tid - %msg%n
        </pattern>
    </layout>
</appender>
```

## canal
* 基于数据库增量日志解析，提供增量数据订阅&消费
* 测试时修改配置文件 instance.properties
```
canal.mq.topic=ds
```

## XXL-JOB
* XXL-JOB是一个分布式任务调度平台，其核心设计目标是开发迅速、学习简单、轻量级、易扩展

## Sentinel
* Sentinel 提供一个轻量级的开源控制台，它提供机器发现以及健康情况管理、监控（单机和集群），规则管理和推送的功能
* [网关限流介绍](https://github.com/alibaba/Sentinel/wiki/%E7%BD%91%E5%85%B3%E9%99%90%E6%B5%81#%E7%BD%91%E5%85%B3%E6%B5%81%E6%8E%A7%E6%8E%A7%E5%88%B6%E5%8F%B0)
* 启动命令：java -Dserver.port=8080 -Dcsp.sentinel.dashboard.server=localhost:8080 -Dproject.name=sentinel-dashboard -jar sentinel-dashboard.jar