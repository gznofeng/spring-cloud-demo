package com.example.job.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name="service-luna")
public interface ServiceLunaClient {

    @PostMapping("/service/info")
    String info();
}
