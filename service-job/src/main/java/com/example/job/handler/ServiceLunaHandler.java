package com.example.job.handler;

import com.example.job.client.ServiceLunaClient;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class ServiceLunaHandler {

    @Resource
    private ServiceLunaClient serviceLunaClient;

    /**
     * 1、简单任务示例（Bean模式）
     */
    @XxlJob("serviceLunaJobHandler")
    public ReturnT<String> serviceLunaJobHandler(String param) throws Exception {
        XxlJobLogger.log("param:{}", param);
        serviceLunaClient.info();
        return ReturnT.SUCCESS;
    }
}
