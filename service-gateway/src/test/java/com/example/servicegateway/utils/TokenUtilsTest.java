package com.example.servicegateway.utils;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

@Slf4j
public class TokenUtilsTest {

    @Test
    public void testCreateToken() {
        Integer uid = 999;
        TokenUtils tokenUtils = new TokenUtils();
        String token = tokenUtils.createToken(uid);
        log.info("token:{}", token);
    }
}
