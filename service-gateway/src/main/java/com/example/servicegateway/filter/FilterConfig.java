package com.example.servicegateway.filter;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilterConfig {

    @Bean
    public TokenGatewayFilterFactory validGatewayFilterFactory() {
        return new TokenGatewayFilterFactory();
    }
}
