package com.example.servicegateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * Spring Cloud Gateway集成Sentinel：https://blog.csdn.net/plgy_Y/article/details/100316177
 *
 * 启动参数：-Dcsp.sentinel.app.type=1
 * 测试链接：http://localhost:8764/gateway/service/info
 */
@EnableDiscoveryClient
@SpringBootApplication
public class ServiceGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceGatewayApplication.class, args);
    }

}
