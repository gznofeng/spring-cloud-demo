//package com.example.servicegateway.route.locator;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.JSONObject;
//import com.example.servicegateway.utils.TokenUtils;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.commons.collections4.CollectionUtils;
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.cloud.gateway.route.RouteLocator;
//import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.MediaType;
//import reactor.core.publisher.Mono;
//
//import javax.annotation.Resource;
//import java.util.List;
//
///**
// * 定义路由定位器，对请求body拦截处理，因为springcloud配置无法处理请求body，必须用代码实现
// * 修改request body对象配置
// */
//@Slf4j
//@Configuration
//public class ModifyReqBodyRouteLocator {
//
//    @Resource
//    private TokenUtils tokenUtils;
//
//    @Bean
//    public RouteLocator routes(RouteLocatorBuilder builder) {
//        log.info("初始化RouteLocator");
//        return builder.routes()
//                .route(r -> r.path("/gateway/**") // 设置路由断言，以gateway开头url进行拦截
//                             .filters(f -> f.rewritePath("/gateway", "") //拦截后将gateway路径替换
//                                            .modifyRequestBody(String.class,
//                                                               String.class,
//                                                               (exchange, s) -> {
//                                                                   final HttpHeaders headers = exchange.getRequest().getHeaders();
//                                                                   final MediaType contentType = headers.getContentType();
//                                                                   log.info("contentType:{}", contentType);
//                                                                   if (contentType != null && MediaType.APPLICATION_JSON_VALUE.equals(contentType.toString())) {
//                                                                       final Integer uid = getUid(headers);
//                                                                       return Mono.just(modify(s, uid));
//                                                                    } else {
//                                                                       log.info("contentType != application/json 直接返回空字符串");
//                                                                       return Mono.just(StringUtils.EMPTY);
//                                                                    }
//                                                               }
//                                            ) // request body进行处理
//                             ).uri("lb://service-luna")
//                ).build();
//    }
//
//    private String modify(String messge, Integer uid) {
//        log.info("待修改body信息：{}", messge);
//        if (StringUtils.isBlank(messge)) {
//            return StringUtils.EMPTY;
//        }
//
//        JSONObject jsonObject = JSON.parseObject(messge);
//        jsonObject.put("userId", uid);
//        return jsonObject.toJSONString();
//    }
//
//    private Integer getUid(HttpHeaders headers) {
//        Integer uid = null;
//        final List<String> tokens = headers.get("token");
//        if (CollectionUtils.isNotEmpty(tokens)) {
//            final String token = tokens.get(0);
//            tokenUtils.validateToken(token);
//            uid = tokenUtils.getUidFromToken(token);
//        }
//        return uid;
//    }
//}
