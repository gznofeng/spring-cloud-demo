package com.demo.dto;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 通用的返回结果
 * 不用每次都写新的返回结果
 */
@Data
@ApiModel(value = "GeneralResult", description = "通用返回结果")
public class GeneralResult<T> {
    @ApiModelProperty(value = "响应编码", required = true)
    protected int code;

    @ApiModelProperty(value = "响应的详细信息", required = true)
    protected String msg;

    /**
     * 返回数据
     */
    private T data;

    public static <T> GeneralResult<T> success(T t) {
        GeneralResult<T> generalResult = new GeneralResult<>();
        generalResult.setData(t);
        return generalResult;
    }

    public static GeneralResult<Void> success() {
        return new GeneralResult<>();
    }

    public static <T> GeneralResult<T> error(String msg) {
        GeneralResult<T> generalResult = new GeneralResult<>();
        generalResult.setCode(1);
        generalResult.setMsg(msg);
        return generalResult;
    }

    public static <T> GeneralResult<T> error(String msg, T t) {
        GeneralResult<T> generalResult = new GeneralResult<>();
        generalResult.setCode(1);
        generalResult.setMsg(msg);
        generalResult.setData(t);
        return generalResult;
    }


    public static <T> GeneralResult<T> error(Integer code, String msg) {
        GeneralResult<T> generalResult = new GeneralResult<>();
        generalResult.setCode(1);
        generalResult.setMsg(msg);
        return generalResult;
    }

    public static <T> GeneralResult<T> error(Integer code, String msg, T t) {
        GeneralResult<T> generalResult = new GeneralResult<>();
        generalResult.setCode(code);
        generalResult.setMsg(msg);
        generalResult.setData(t);
        return generalResult;
    }


}
