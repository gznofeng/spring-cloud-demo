package com.demo.controller;

import com.demo.service.HttpClientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;

@Slf4j
@RestController
public class HttpClientController {

    @Resource
    private HttpClientService httpClientService;

    /**
     * 验证webclient异步请求
     * @return
     */
    @GetMapping("/asyncSubscribe")
    public Date asyncSubscribe() {
        httpClientService.asyncSubscribe();
        log.info("asyncSubscribe 请求完毕");
        return new Date();
    }

    @GetMapping("/asyncSubscribeException")
    public Date asyncSubscribeException() throws Exception {
        httpClientService.asyncSubscribeException();
        log.info("asyncSubscribeException 请求完毕");
        return new Date();
    }
}
