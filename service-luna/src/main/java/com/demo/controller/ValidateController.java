package com.demo.controller;

import com.alibaba.fastjson.JSON;
import com.demo.dto.GatewayReq;
import com.demo.model.Order;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
//@Validated
public class ValidateController {

    @PostMapping("/validate/simple/obj")
    public String validate(@RequestBody @Validated GatewayReq req) {
        log.info("validate对象：{}", JSON.toJSONString(req));
        return JSON.toJSONString(req);
    }

    /**
     * 如果要校验集合里的对象，被校验的参数必须使用@Valid，类必须加上@Validated
     * @param reqs
     * @return
     */
    @PostMapping("/validate/list/obj")
    public String validateList(@RequestBody @Valid List<GatewayReq> reqs) {
        log.info("validate集合：{}", JSON.toJSONString(reqs));
        return JSON.toJSONString(reqs);
    }

    @ApiOperation(value = "验证对象两个字段不能同时为空")
    @PostMapping("/validate/group")
    public Boolean validateGroup(@RequestBody @Validated Order order) {
        log.info(order.toString());
        return true;
    }
}
