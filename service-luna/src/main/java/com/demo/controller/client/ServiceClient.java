package com.demo.controller.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(contextId = "lunaClient",name = "service-luna")
public interface ServiceClient {

    @GetMapping("/testFeign")
    String testFeign();
}
