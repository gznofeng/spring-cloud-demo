package com.demo.controller;

import com.demo.service.ThreadService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@RestController
public class ThreadController {

    @Resource
    private ThreadService threadService;

    @GetMapping("/getThreadResult")
    public List<String> getThreadResult() throws ExecutionException, InterruptedException {
        List<String> list = new ArrayList<>();
        List<Future<String>> futures = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            Future<String> threadResult = threadService.getThreadResult(i);
            futures.add(threadResult);
        }

        for (Future<String> future : futures) {
            list.add(future.get());
        }
        return list;
    }
}
