package com.demo.controller;

import com.alibaba.fastjson.JSON;
import com.demo.controller.client.ServiceClient;
import com.demo.dto.GatewayReq;
import com.demo.dto.GeneralResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 测试链接： http://localhost:8763/hi?name=forezp
 * @author K
 *
 */
@RestController
// Spring Bean重新初始化并载入新的配置内容，修改配置文件后不用重启服务重新加载
// 需要动态更新配置信息，只要在提供服务的类添加@RefreshScope
@RefreshScope
public class ServicController implements ServiceClient {

	private Logger logger = LoggerFactory.getLogger(getClass());

	private AtomicInteger atomicInteger = new AtomicInteger();

	@Value("${server.port}")
	String port;

	// 读取配置中心属性
	@Value("${foo}")
	String foo;

	// 测试读取配置中心的配置
	@PostMapping("/service/hi")
	public String hi(@RequestParam String name) {
		String hi = "hi " + name + ",i am from port:" + port + ", config:" + foo;
		logger.info("/hi服务：{}", hi);
		return hi;
	}

	@GetMapping("/service/info")
    public String info(){
		logger.info("servic-hi服务info1接口被调用,调用次数：{}", atomicInteger.incrementAndGet());
		return "i'm service-hi info " + new Date();
    }

	@PostMapping("/service/testGateway")
	public GeneralResult<GatewayReq> testGateway(@RequestBody GatewayReq gatewayReq) {
		logger.info("testGateWay 入参：{}", JSON.toJSONString(gatewayReq));
		return GeneralResult.success(gatewayReq);
	}

	@Override
	public String testFeign() {
		return "testFegin";
	}
}
