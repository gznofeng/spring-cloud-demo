package com.demo.state.machine.event;

public enum Event {
    SAVE,
    PASS,
    UN_PASS
}
