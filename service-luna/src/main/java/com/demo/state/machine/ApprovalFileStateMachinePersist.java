package com.demo.state.machine;

import com.demo.model.ApprovalFile;
import com.demo.state.machine.event.Event;
import com.demo.state.machine.status.Status;
import org.springframework.statemachine.StateMachineContext;
import org.springframework.statemachine.StateMachinePersist;
import org.springframework.statemachine.StateMachineSystemConstants;
import org.springframework.statemachine.support.DefaultStateMachineContext;
import org.springframework.stereotype.Component;

/**
 * 设置一个伪状态持久化类，read方法通过获取入参参数将状态赋值到当前状态机
 */
@Component
public class ApprovalFileStateMachinePersist implements StateMachinePersist<Status, Event, ApprovalFile> {

    @Override
    public void write(StateMachineContext<Status, Event> context, ApprovalFile contextObj) throws Exception {

    }

    @Override
    public StateMachineContext<Status, Event> read(ApprovalFile contextObj) throws Exception {
        return new DefaultStateMachineContext<>(Status.valueOf(contextObj.getStatus().name()),
                null, null, null, null, StateMachineSystemConstants.DEFAULT_ID_STATEMACHINEFACTORY);
    }
}
