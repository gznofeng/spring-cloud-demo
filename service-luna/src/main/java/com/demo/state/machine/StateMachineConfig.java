package com.demo.state.machine;

import com.demo.state.machine.event.Event;
import com.demo.state.machine.status.Status;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachine;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import java.util.EnumSet;

/**
 * 状态机配置
 * 使用状态必须把延迟初始化关闭spring.main.lazy-initialization=false。
 * 因为StateMachineHandlerCallHelper.afterPropertiesSet启动会扫描使用@WithStateMachine的类，并且存放在map里
 */
@Configuration
@EnableStateMachine
public class StateMachineConfig extends EnumStateMachineConfigurerAdapter<Status, Event> {

    @Override
    public void configure(StateMachineStateConfigurer<Status, Event> states) throws Exception {
        states.withStates().initial(Status.DRAFT).states(EnumSet.allOf(Status.class));
    }

    @Override
    public void configure(StateMachineTransitionConfigurer<Status, Event> transitions) throws Exception {
        transitions.withExternal()
                .source(Status.DRAFT).target(Status.AUDIT)
                .event(Event.SAVE)
                .and()
                .withExternal()
                .source(Status.AUDIT).target(Status.FINISH)
                .event(Event.PASS)
                .and()
                .withExternal()
                .source(Status.AUDIT).target(Status.DRAFT)
                .event(Event.UN_PASS);
    }
}
