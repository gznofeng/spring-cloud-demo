package com.demo.state.machine.status;

public enum Status {
    DRAFT,
    AUDIT,
    FINISH
}
