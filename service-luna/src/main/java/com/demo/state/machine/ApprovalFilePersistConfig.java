package com.demo.state.machine;

import com.demo.model.ApprovalFile;
import com.demo.state.machine.event.Event;
import com.demo.state.machine.status.Status;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.persist.DefaultStateMachinePersister;
import org.springframework.statemachine.persist.StateMachinePersister;

import javax.annotation.Resource;

@Configuration
public class ApprovalFilePersistConfig {

    @Resource
    private ApprovalFileStateMachinePersist approvalFileStateMachinePersist;

    @Bean(name="approvalFilePersister")
    public StateMachinePersister<Status, Event, ApprovalFile> orderPersister() {
        return new DefaultStateMachinePersister<>(approvalFileStateMachinePersist);
    }
}
