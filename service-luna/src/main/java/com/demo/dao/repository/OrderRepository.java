package com.demo.dao.repository;

import com.demo.model.Order;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Lazy
@Repository
public interface OrderRepository extends ElasticsearchRepository<Order, Long> {

    List<Order> findByOrderSnLike(String orderSn);
}
