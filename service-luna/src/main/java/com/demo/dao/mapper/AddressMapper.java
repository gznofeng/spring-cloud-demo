package com.demo.dao.mapper;

import com.demo.model.Address;
import com.demo.model.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AddressMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Address record);

    int insertSelective(Address record);

    Address selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Address record);

    int updateByPrimaryKey(Address record);

    @ResultMap("BaseResultMap")
    @Select("select user_id, user_address from address where user_id = #{userId}")
    List<Address> queryByUserId(@Param("userId") Long userId);
}