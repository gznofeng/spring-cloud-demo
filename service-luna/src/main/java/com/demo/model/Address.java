package com.demo.model;

import java.io.Serializable;
import lombok.Data;

/**
 * address
 * @author 
 */
@Data
public class Address implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 用户地址
     */
    private String userAddress;

    private static final long serialVersionUID = 1L;
}