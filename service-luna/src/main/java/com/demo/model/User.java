package com.demo.model;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author 
 */
@Data
public class User implements Serializable {
    private Long id;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 用户名
     */
    private String userName;

    List<Address> addressList;

    private static final long serialVersionUID = 1L;
}