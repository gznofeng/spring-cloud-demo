package com.demo.model;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.validation.constraints.AssertFalse;
import javax.validation.constraints.AssertTrue;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Document(indexName = "luna", type = "order")
public class Order {
    @Id
    private Long orderId;
    private String orderSn;
    private String address;
    // 购买日期
    private Date dateOfPurchase;
    private BigDecimal price;
    // 固话
    private String tel;
    // 手机
    private String mobileTel;

    @AssertFalse(message = "tel,mobile字段不能同时为空")
    private boolean isValid() {
        return StringUtils.isAllBlank(tel, mobileTel);
    }
}
