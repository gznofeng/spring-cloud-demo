package com.demo.model;

import com.demo.state.machine.status.Status;
import lombok.Data;

@Data
public class ApprovalFile {

    private Status status;
}
