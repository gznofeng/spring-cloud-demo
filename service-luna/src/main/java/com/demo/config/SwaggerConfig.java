package com.demo.config;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.springframework.boot.SpringBootVersion;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.RequestParameterBuilder;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.service.ParameterType;
import springfox.documentation.service.RequestParameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.List;

/**
 * Swagger3配置类
 * Swagger3配置不能使用延迟加载，使用延迟加载后，导致ui界面body不能显示参数名
 * @Configuration：让Spring来加载该类配置。
 * @EnableOpenApi：启用Swagger3。
 *
 * springfox 3.0 url http://localhost:8763/swagger-ui/index.html
 */
@Configuration
@EnableOpenApi
public class SwaggerConfig {

    /**
     * 创建API应用
     * 通过select()函数返回一个ApiSelectorBuilder实例,用来控制哪些接口暴露给Swagger来展现，
     * 本例采用指定扫描的包路径来定义指定要建立API的目录。
     * @return
     */
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.OAS_30).pathMapping("/")
                // 定义是否开启swagger，false为关闭，可以通过变量控制
                .enable(true)
                // 将api的元信息设置为包含在json ResourceListing响应中。
                .apiInfo(apiInfo())
                // 选择哪些接口作为swagger的doc发布
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.demo"))
                .paths(PathSelectors.any())
                .build()
                // 全局请求参数
                .globalRequestParameters(getGlobalOperationParameters())
                // 支持的通讯协议集合
                .protocols(Sets.newHashSet("https", "http"));
    }

    /**
     * API 页面上半部分展示信息
     */
    private ApiInfo apiInfo()
    {
        return new ApiInfoBuilder().title("service-luna Api Doc")
                .version("Application Version: 0.0.1, Spring Boot Version: " + SpringBootVersion.getVersion())
                .build();
    }

    /**
     * 全局请求参数
     * @return
     */
    private List<RequestParameter> getGlobalOperationParameters() {
        RequestParameterBuilder builder = new RequestParameterBuilder();
        RequestParameter parameter =
                builder.name("token").description("用户token").in(ParameterType.HEADER).required(false).build();

        return Lists.newArrayList(parameter);
    }
}