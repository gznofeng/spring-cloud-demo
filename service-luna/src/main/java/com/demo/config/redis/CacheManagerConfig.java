package com.demo.config.redis;

import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.RedisSerializer;

public class CacheManagerConfig {

    public static RedisCacheConfiguration getConfig() {
        return RedisCacheConfiguration.defaultCacheConfig()
                .computePrefixWith((cacheName -> cacheName))
                .disableCachingNullValues()         //如果是空值，不缓存
                .serializeValuesWith(RedisSerializationContext.SerializationPair.fromSerializer((valueSerializer())));
    }

    private static RedisSerializer<Object> valueSerializer() {
        return new Jackson2JsonRedisSerializer(Object.class);
    }
}
