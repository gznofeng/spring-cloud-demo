package com.demo;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * 测试链接：http://localhost:8763/hi?name=forezp
 * @author K
 *
 */
@EnableAsync
@EnableCaching
@ServletComponentScan
@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.demo.dao.mapper")
public class ServiceLunaApplication {

	private static Logger log = LoggerFactory.getLogger(ServiceLunaApplication.class);
	
	public static void main(String[] args) {
		log.debug("service-luna startup............................................");
		SpringApplication.run(ServiceLunaApplication.class, args);
	}

}
