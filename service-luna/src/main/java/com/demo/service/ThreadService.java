package com.demo.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.util.concurrent.Future;

@Slf4j
@Service
public class ThreadService {

    @Async("taskExecutor")
    public Future<String> getThreadResult(int i) {
        String str = "getThreadResult, i = " + i;
        log.info("线程：{}， i = {}", Thread.currentThread().getName(), i);
        return new AsyncResult<>(str);
    }
}
