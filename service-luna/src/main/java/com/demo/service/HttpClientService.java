package com.demo.service;

import com.alibaba.fastjson.JSON;
import com.demo.dao.mapper.UserMapper;
import com.demo.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class HttpClientService {

    @Resource
    private UserMapper userMapper;

    @Resource
    private WebClient webClient;

    public User asyncRequest() {
        User user = new User();
        user.setId(666L);

        Mono<User> userMono = webClient.post().uri("http://localhost:8989/respBody")
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(user).retrieve().bodyToMono(User.class);
        log.info("请求已发送");
        return userMono.block();
    }

    public void asyncRequestException() {
        User user = new User();
        user.setId(666L);

        Mono<User> userMono = webClient.post().uri("http://localhost:8989/respBodyException")
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(user).retrieve().bodyToMono(User.class);
        log.info("请求已发送");
        User block = userMono.block();
        log.info(JSON.toJSONString(block));
    }

    public void batchAsyncRequest() {
        List<Mono<User>> postResults = new ArrayList<>();
        List<Mono<String>> getResults = new ArrayList<>();

        for (long i = 0; i < 10; i++) {
            if (i % 2 == 0) {
                User user = new User();
                user.setUserId(i);
                Mono<User> userMono = webClient.post().uri("http://localhost:8989/respBody")
                        .accept(MediaType.APPLICATION_JSON)
                        .bodyValue(user).retrieve().bodyToMono(User.class);
                postResults.add(userMono);
            } else {
                Mono<String> stringMono =
                        webClient.get().uri("http://localhost:8989/hi").retrieve().bodyToMono(String.class);

                getResults.add(stringMono);
            }
        }
        log.info("请求已发送");

        for (Mono<String> mono : getResults) {
            log.info("getResult:{}", mono.block());
        }

        for (Mono<User> mono : postResults) {
            log.info("postResult:{}", mono.block());
        }
    }

    public User asyncSubscribe() {
        User user = new User();
        user.setId(666L);

        Mono<User> userMono = webClient.post().uri("http://localhost:8989/respBody")
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(user).retrieve().bodyToMono(User.class);
        log.info("请求已发送");
        userMono.subscribe(s -> log.info(JSON.toJSONString(s)));
        return user;
    }

    @Transactional(rollbackFor = Exception.class)
    public void asyncSubscribeException() throws Exception {
        User user = asyncSubscribe();
        userMapper.insertSelective(user);
        throw new Exception("异常");
    }
}
