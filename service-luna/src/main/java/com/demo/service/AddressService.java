package com.demo.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Slf4j
@Service
public class AddressService {

    @Resource
    private DbShardingStrategyService dbShardingStrategyService;

    public void saveAddress() {
        dbShardingStrategyService.insertAfterPublisherForAddr(System.currentTimeMillis());
        log.info("saveAddress");
    }
}
