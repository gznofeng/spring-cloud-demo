package com.demo.jdk.thread;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.CountDownLatch;

@Slf4j
public class TestRunable implements Runnable {

    private int i;
    private CountDownLatch countDownLatch;

    public TestRunable(int i, CountDownLatch countDownLatch) {
        this.i = i;
        this.countDownLatch = countDownLatch;
    }

    @Override
    public void run() {
        try {
//            countDownLatch.await();
            log.info(Thread.currentThread().getName() + ", i = " + i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
