package com.demo.jdk.thread;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ScheduleRunable implements Runnable {

    private int i;

    public ScheduleRunable(int i) {
        this.i = i;
    }

    @Override
    public void run() {
        try {
            log.info(Thread.currentThread().getName() + ", i = " + i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
