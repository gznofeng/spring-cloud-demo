package com.demo.jdk.lambda;

import com.alibaba.fastjson.JSON;
import com.demo.model.Person;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Slf4j
public class LambdaPredicate {

    /**
     * 对象集合有相同值去重
     */
    public void distinct() {
        List<Person> list = new ArrayList<>();

        Person person = new Person();
        person.setName("aa");
        list.add(person);

        person = new Person();
        person.setName("aa");
        list.add(person);

        person = new Person();
        person.setName("bb");
        list.add(person);

        List<Person> people = list.stream().filter(distinctByKey(p -> p.getName())).collect(Collectors.toList());
        log.info("LambdaPredicate.distinct:{}", JSON.toJSONString(people));
    }

    private <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
        // 这个函数将应用到每一个item
        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }
}
