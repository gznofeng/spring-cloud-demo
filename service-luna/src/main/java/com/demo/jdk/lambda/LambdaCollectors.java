package com.demo.jdk.lambda;

import com.alibaba.fastjson.JSON;
import com.demo.model.Person;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
public class LambdaCollectors {

    /**
     * List转map，value是List对象
     */
    public void groupBy() {
        List<Person> list = new ArrayList<>();

        Person person = new Person();
        person.setName("aa");
        list.add(person);

        person = new Person();
        person.setName("aa");
        list.add(person);

        person = new Person();
        person.setName("bb");
        list.add(person);

        Map<String, List<Person>> map = list.stream().collect(Collectors.groupingBy(Person::getName));
        log.info("Collectors.groupingBy：{}", JSON.toJSONString(map));
    }
}
