package com.demo.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 分片注解，使用注解时两个属性不能同时赋值空字符或空值
 * 如果两个属性同时有值，优先使用shardingColumn
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface ShardingStrategy {

    /**
     * 分片值，shardingColumn赋值入参参数名，获取入参的值进行取模，获取指定db
     * @return
     */
    String shardingColumn() default "";

    /**
     * 指定db，获取入参的值得到指定db
     * @return
     */
    String dbIndex() default "";
}
