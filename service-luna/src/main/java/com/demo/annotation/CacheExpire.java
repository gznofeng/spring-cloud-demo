package com.demo.annotation;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface CacheExpire {

    /**
     * 超时时间, 默认超时60s
     */
    @AliasFor("expire")
    long value() default 60L;

    /**
     * 超时时间, 默认超时60s
     */
    @AliasFor("value")
    long expire() default 60L;
}
