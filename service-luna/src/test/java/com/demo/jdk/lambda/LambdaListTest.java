package com.demo.jdk.lambda;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.List;

@Slf4j
public class LambdaListTest {

    @Test
    public void testDoubleForeach() {
        LambdaList lambdaList = new LambdaList();
        List<String> strings = lambdaList.doubleForeach();
        log.info(strings.toString());

        strings = lambdaList.doubleForeach1();
        log.info(strings.toString());
    }
}
