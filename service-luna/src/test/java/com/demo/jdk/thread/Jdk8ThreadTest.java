package com.demo.jdk.thread;

import org.junit.Test;

import java.util.concurrent.ExecutionException;

public class Jdk8ThreadTest {

    @Test
    public void testCompletableFuture() throws ExecutionException, InterruptedException {
        Jdk8Thread jdk8Thread = new Jdk8Thread();
        jdk8Thread.completableFuture();
    }

    @Test
    public void completableFutureExecutors() throws ExecutionException, InterruptedException {
        Jdk8Thread jdk8Thread = new Jdk8Thread();
        jdk8Thread.completableFutureExecutors();
    }
}
