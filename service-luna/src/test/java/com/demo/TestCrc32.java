package com.demo;

import java.util.zip.CRC32;

public class TestCrc32 {

    public static void main(String[] args) {
        CRC32 crc32 = new CRC32();
        crc32.update("abc".getBytes());
        long value = crc32.getValue();
        System.out.println(value % 2L);

        crc32 = new CRC32();
        crc32.update("5".getBytes());
        value = crc32.getValue();
        System.out.println("value = " + value);
        System.out.println(value % 2L);
    }
}
