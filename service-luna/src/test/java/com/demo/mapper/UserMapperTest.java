package com.demo.mapper;

import com.demo.dao.mapper.UserMapper;
import com.demo.model.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.shardingsphere.api.hint.HintManager;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserMapperTest {

    @Resource
    private UserMapper userMapper;

    @Transactional(rollbackFor = Exception.class)
    @Rollback
    @Test
    public void testInsert() {

        try (HintManager instance = HintManager.getInstance();) {
            instance.setDatabaseShardingValue("ds0");
//            HintManagerHolder.clear();
//            HintManagerHolder.setHintManager(instance);

            for (long i = 0; i < 10; i++) {
//                log.info("数值：{}，取模：{}", i, i%2);
                User user = new User();
                user.setUserId(i);
                userMapper.insertSelective(user);
            }

            for (long i = 0; i < 10; i++) {
                log.info("userId = {}", i);
                List<User> users = userMapper.queryByUserId(i);
                User user = users.get(0);
                Assert.assertEquals(i, user.getUserId().longValue());
            }
        }
    }
}
