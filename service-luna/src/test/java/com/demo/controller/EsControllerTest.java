package com.demo.controller;

import com.demo.dao.repository.OrderRepository;
import com.demo.model.Order;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class EsControllerTest {

    @Resource
    private OrderRepository orderRepository;

    /**
     * 初始化es数据，测试分页
     */
    @Test
    public void initEsData() {
        List<Order> list = new ArrayList<>();
        for (int i = 0; i < 15000; i++) {
            Order order = new Order();
            order.setOrderId((long) i);
            list.add(order);

            if (i % 1000 == 1) {
                orderRepository.saveAll(list);
                list.clear();
            }
        }

        if (CollectionUtils.isNotEmpty(list)) {
            orderRepository.saveAll(list);
        }
    }
}
