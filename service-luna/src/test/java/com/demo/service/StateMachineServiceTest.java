package com.demo.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class StateMachineServiceTest {

    @Resource
    private StateMachineService stateMachineService;

    @Test
    public void testMachineStart() throws InterruptedException {
        stateMachineService.machineStart();
    }

    /**
     * 测试单个状态并发执行，结果不支持并发
     * @throws ExecutionException
     * @throws InterruptedException
     */
    @Test
    public void parallelMachineStart() throws ExecutionException, InterruptedException {
        int num = 30;
        ExecutorService executorService = Executors.newWorkStealingPool(num);
        List<CompletableFuture<Boolean>> list = new ArrayList<>();

        CountDownLatch countDownLatch = new CountDownLatch(1);
        for (int i = 0; i < num; i++) {
            CompletableFuture<Boolean> mapCompletableFuture = CompletableFuture.supplyAsync(() -> {
                try {
                    countDownLatch.await();
                    stateMachineService.machineStart();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return true;
            }, executorService);
            list.add(mapCompletableFuture);
        }

        countDownLatch.countDown();
        for (CompletableFuture<Boolean> mapCompletableFuture : list) {
            Boolean flag = mapCompletableFuture.get();
            log.info("flag:{}", flag);
        }
    }

    @Test
    public void parallelMachineFactoryStart() throws ExecutionException, InterruptedException {
        int num = 30;
        ExecutorService executorService = Executors.newWorkStealingPool(num);
        List<CompletableFuture<Boolean>> list = new ArrayList<>();

        CountDownLatch countDownLatch = new CountDownLatch(1);
        for (int i = 0; i < num; i++) {
            CompletableFuture<Boolean> mapCompletableFuture = CompletableFuture.supplyAsync(() -> {
                try {
                    countDownLatch.await();
                    stateMachineService.machineFactoryStart();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return true;
            }, executorService);
            list.add(mapCompletableFuture);
        }

        countDownLatch.countDown();
        for (CompletableFuture<Boolean> mapCompletableFuture : list) {
            Boolean flag = mapCompletableFuture.get();
            log.info("flag:{}", flag);
        }
    }

    @Test
    public void testChangeMachineStatus() throws Exception {
        stateMachineService.changeMachineStatus();
    }
}
