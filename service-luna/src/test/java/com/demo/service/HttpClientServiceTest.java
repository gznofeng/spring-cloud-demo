package com.demo.service;

import com.demo.model.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class HttpClientServiceTest {

    @Resource
    private HttpClientService httpClientService;

    @Test
    public void testAsyncRequest() throws InterruptedException {
        User user = httpClientService.asyncRequest();
        log.info("finish");
        Assert.assertEquals(1L, user.getId().longValue());
    }

    @Test(expected = Exception.class)
    public void testAsyncRequestException() {
        httpClientService.asyncRequestException();
        log.info("finish");
    }

    @Test
    public void testBatchAsyncRequest() throws InterruptedException {
        httpClientService.batchAsyncRequest();
    }
}
