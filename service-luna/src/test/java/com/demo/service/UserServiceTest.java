package com.demo.service;

import com.alibaba.fastjson.JSON;
import com.demo.model.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    @Resource
    private UserService userService;

    @Test
    public void testSaveUser() {
        userService.saveUser();
        log.info("testSaveUser 执行完毕");
    }

    @Test
    public void testGetUser() {
        List<User> user = userService.getUser(1595493634122L);
        userService.getUserByUserId(1595493634122L);
        log.info(JSON.toJSONString(user));
    }

    @Test
    public void testReturnNull() {
        List<User> user = userService.returnNull(1595493634122L);
        log.info(JSON.toJSONString(user));
    }

    @Test
    public void testDeleteCache() {
        userService.deleteCache(1595493634122L);
    }
}
