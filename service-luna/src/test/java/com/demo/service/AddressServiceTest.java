package com.demo.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class AddressServiceTest {

    @Resource
    private AddressService addressService;

    @Test
    public void testSaveAddress() throws InterruptedException {
        addressService.saveAddress();
        log.info("testSaveAddress 执行完毕, 进入线程阻塞");
        Thread.sleep(6000L);
        log.info("finish");
    }
}
