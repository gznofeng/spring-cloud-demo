package com.demo.service;

import com.demo.model.Address;
import com.demo.model.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class DbShardingStrategyServiceTest {

    @Resource
    private DbShardingStrategyService dbShardingStrategyService;

    @Transactional(rollbackFor = Exception.class)
    @Rollback
    @Test
    public void insertByShardingValue() {
        Long userId = 11L;
        log.info("userId = {}, 取模：{}", userId, userId%2);
        dbShardingStrategyService.insertByShardingValue(userId);
        List<User> users = dbShardingStrategyService.selectByUserId(userId);
        Assert.assertEquals(1, users.size());
    }

    @Transactional(rollbackFor = Exception.class)
    @Rollback
    @Test
    public void insertByShardingValue1() {
        for (long i = 0; i < 10; i++) {
            log.info("i = {}, 取模：{}", i, i%2);
            dbShardingStrategyService.insertByShardingValue(i);
            List<User> users = dbShardingStrategyService.selectByUserId(i);
            Assert.assertEquals(i, users.get(0).getUserId().longValue());

            final List<Address> addresses = dbShardingStrategyService.selectByAddrUserId(i);
            Assert.assertEquals(i, addresses.get(0).getUserId().longValue());
        }
    }
}
